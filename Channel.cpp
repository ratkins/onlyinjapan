#ifndef Channel_h
#define Channel_h

#include "FastLED.h"
#include "TestPattern.cpp"
#include "SolidBars.cpp"
#include "Heights.cpp"
#include "Fire.cpp"
#include "Twinkles.cpp"
#include "Drips.cpp"
		
#define PROGRAM_COUNT 7
#define HEIGHT 80
    
class Channel {
  
  private:
    CRGB* leds;
    uint8_t numLeds;
    uint8_t channel;
    
    CRGBPalette16** palettes;
    uint8_t numPalettes;    
    uint8_t currentPalette;

    TestPattern testPattern;
    SolidBars solidBars;
    Heights heights;
    Twinkles twinkles;
    Fire fire;
    Drips dripsDown = Drips(true);
    Drips dripsUp = Drips(false);

    Program* programs[PROGRAM_COUNT] = {
      &testPattern,
      &solidBars,
      &heights,
      &twinkles,
      &fire,
      &dripsDown,
      &dripsUp,
    };
    uint8_t currentProgram;    

  public:
    Channel(uint8_t channel, CRGB* leds, uint8_t numLeds, CRGBPalette16** palettes, uint8_t numPalettes) 
    : channel(channel), leds(leds), numLeds(numLeds), palettes(palettes), numPalettes(numPalettes), currentPalette(0), currentProgram(0) {}

    void draw() {
      programs[currentProgram]->draw(leds, numLeds, (*palettes)[currentPalette]); // or is it just *palettes[currentPalette] ???
      
      // If we're doing the test pattern, turn on the identifier lights
      if (currentProgram == 0) {
        for (int i = 0; i < channel + 1; i++) {
          leds[i] = CRGB::White;
          leds[i + HEIGHT] = CRGB::White;
        }
      }
    }
    
    void noteOn(byte note, byte velocity) {
      programs[currentProgram]->noteOn(note, velocity);
    }
    
    void noteOff(byte note, byte velocity) {
      programs[currentProgram]->noteOff(note, velocity); 
    }

    void velocityChange(byte note, byte velocity) {
      programs[currentProgram]->velocityChange(note, velocity);
    }

    void controlChange(byte control, byte value) {
        // CC 0 is "bank select", aka "palette select"
        if (control == 0) {
          currentPalette = value % numPalettes;
        } else {
          programs[currentProgram]->controlChange(control, value);
        }
    }

    void programChange(byte program) {
      currentProgram = program % PROGRAM_COUNT;
    }

    void afterTouch(byte pressure) {
      programs[currentProgram]->afterTouch(pressure);      
    }

    void pitchChange(int pitch) {
      programs[currentProgram]->pitchChange(pitch);      
    }

};

#endif
