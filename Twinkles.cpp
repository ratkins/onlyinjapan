#ifndef Twinkles_h
#define Twinkles_h

#include "FastLED.h"
#include "Program.cpp"

#define FADE_IN_SPEED       32
#define FADE_OUT_SPEED      20

#define NUM_LEDS 160 // I just happen to know this...

class Twinkles : public Program {
  
  enum { GETTING_DARKER = 0, GETTING_BRIGHTER = 1 };
  
  private:
    uint8_t startingBrightness;
    uint8_t density;
    boolean on;
  
  public:
    Twinkles() : on(false), startingBrightness(64), density(255) {}
    
    void draw(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) {
      // Make each pixel brighter or darker, depending on
      // its 'direction' flag.
      brightenOrDarkenEachPixel(leds, numLeds, FADE_IN_SPEED, FADE_OUT_SPEED);

      // Now consider adding a new random twinkle
      if (on && random8() < density) {
        int pos = random16(NUM_LEDS);
        if (!leds[pos]) {
            leds[pos] = ColorFromPalette(palette, random8(), startingBrightness, NOBLEND);
            setPixelDirection(pos, GETTING_BRIGHTER);
        }
      }      
    }
    
    void brightenOrDarkenEachPixel(CRGB* leds, uint8_t numLeds, fract8 fadeUpAmount, fract8 fadeDownAmount) {
      for (uint16_t i = 0; i < numLeds; i++) {
        if (getPixelDirection(i) == GETTING_DARKER) {
          // This pixel is getting darker
          leds[i] = makeDarker(leds[i], fadeDownAmount);
        } else {
          // This pixel is getting brighter
          leds[i] = makeBrighter(leds[i], fadeUpAmount);
          // now check to see if we've maxxed out the brightness
          if (leds[i].r == 255 || leds[i].g == 255 || leds[i].b == 255) {
            // if so, turn around and start getting darker
            setPixelDirection(i, GETTING_DARKER);
          }
        }
      }
    }
    
    CRGB makeBrighter(const CRGB& color, fract8 howMuchBrighter) {
      CRGB incrementalColor = color;
      incrementalColor.nscale8(howMuchBrighter);
      return color + incrementalColor;
    }
 
    CRGB makeDarker(const CRGB& color, fract8 howMuchDarker) {
      CRGB newcolor = color;
      newcolor.nscale8(255 - howMuchDarker);
      return newcolor;
    }
    
    uint8_t  directionFlags[(NUM_LEDS + 7) / 8];
     
    bool getPixelDirection(uint16_t i) {
      uint16_t index = i / 8;
      uint8_t  bitNum = i & 0x07;
      // using Arduino 'bitRead' function; expanded code below
      return bitRead( directionFlags[index], bitNum);
      // uint8_t  andMask = 1 << bitNum;
      // return (directionFlags[index] & andMask) != 0;
    }
     
    void setPixelDirection(uint16_t i, bool dir) {
      uint16_t index = i / 8;
      uint8_t  bitNum = i & 0x07;
      // using Arduino 'bitWrite' function; expanded code below
      bitWrite(directionFlags[index], bitNum, dir);
      //  uint8_t  orMask = 1 << bitNum;
      //  uint8_t andMask = 255 - orMask;
      //  uint8_t value = directionFlags[index] & andMask;
      //  if( dir ) {
      //    value += orMask;
      //  }
      //  directionFlags[index] = value;
    }    
    
    void noteOn(byte note, byte velocity) {
      on = true;
      density = note << 1;
      startingBrightness = velocity << 1;
    }
    
    void noteOff(byte note, byte velocity) {
      on = false;
    }

    void velocityChange(byte note, byte velocity) {
    }

    void controlChange(byte control, byte value) {
    }

    void afterTouch(byte pressure) {
    }

    void pitchChange(int pitch) {
    }

};

#endif
