#ifndef Program_h
#define Program_h

#include "FastLED.h"


class Program {

  public:
      
  Program() {}

    virtual void draw(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) = 0;
 
    virtual void noteOn(byte note, byte velocity) = 0;
    
    virtual void noteOff(byte note, byte velocity) = 0;

    virtual void velocityChange(byte note, byte velocity) = 0;

    virtual void afterTouch(byte pressure) = 0;

    virtual void pitchChange(int pitch) = 0;

    virtual void controlChange(byte control, byte value) = 0;
    
};

#endif




