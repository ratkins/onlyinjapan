#ifndef TestPattern_h
#define TestPattern_h

#include "FastLED.h"
#include "Program.cpp"

#define THROTTLE 128
//#define THROTTLE 16

class TestPattern : public Program {
  
  private:
    boolean on;
    uint16_t time;
  
  public:
    TestPattern() : on(false) {}
    
    void draw(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) {
//      Serial.print("Drawing channel "); Serial.println(channel);

      if (on) {
        time++;
        if (time < THROTTLE * 1) {
          fill_solid(leds, numLeds, CRGB::Red);
        } else if (time < THROTTLE * 2) {
          fill_solid(leds, numLeds, CRGB::Green);
        } else if (time < THROTTLE * 3) {
          fill_solid(leds, numLeds, CRGB::Blue);
        } else if (time < THROTTLE * 4) {
          fill_solid(leds, numLeds, CRGB::White);
        } else {
          time = 0;
        }
      } else {
        fill_solid(leds, numLeds, CRGB::Black);
      }
    }
    
    void noteOn(byte note, byte velocity) {
      on = true;
    }
    
    void noteOff(byte note, byte velocity) {
      on = false;
    }

    void velocityChange(byte note, byte velocity) {
    }

    void controlChange(byte control, byte value) {
    }

    void afterTouch(byte pressure) {
    }

    void pitchChange(int pitch) {
    }

};

#endif



