#include<FastLED.h>

#include "Channel.cpp"

#define NUM_CHANNELS 16
#define NUM_LEDS_PER_CHANNEL 160
#define NUM_LEDS (NUM_CHANNELS * NUM_LEDS_PER_CHANNEL)

#define BRIGHTNESS_PIN 19

#define FRAMES_PER_SECOND 60

CRGB leds[NUM_LEDS];

// Palette definitions
// 
// Palettes are defined here at the top level and a pointer passed in to the channel; the
// palettes are read-only so there only needs to be one copy of each.

TProgmemRGBPalette16 Incandescent_p PROGMEM =
{
  0xE1A024, 0xE1A024, 0xE1A024, 0xE1A024,
  0xE1A024, 0xE1A024, 0xE1A024, 0xE1A024,
  0xE1A024, 0xE1A024, 0xE1A024, 0xE1A024,
  0xE1A024, 0xE1A024, 0xE1A024, 0xE1A024,      
};
CRGBPalette16 incandescent = CRGBPalette16(Incandescent_p);  

TProgmemRGBPalette16 White_p PROGMEM =
{
  0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF,
  0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF,
  0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF,
  0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF
};
CRGBPalette16 white = CRGBPalette16(White_p);  

TProgmemRGBPalette16 WhiteToBlack_p PROGMEM =
{
  0xFFFFFF, 0xEEEEEE, 0xDDDDDD, 0xCCCCCC,
  0xBBBBBB, 0xAAAAAA, 0x999999, 0x888888,
  0x777777, 0x666666, 0x555555, 0x444444,
  0x333333, 0x222222, 0x111111, 0x000000
};
CRGBPalette16 whiteToBlack = CRGBPalette16(WhiteToBlack_p);  

CRGBPalette16 rainbowColors = CRGBPalette16(RainbowColors_p);
CRGBPalette16 oceanColors = CRGBPalette16(OceanColors_p);
CRGBPalette16 partyColors = CRGBPalette16(PartyColors_p);

#define PALETTE_COUNT 6

CRGBPalette16* palettes[] = {
  &rainbowColors,
  &oceanColors,
  &partyColors,      
  &incandescent,
  &white,
  &whiteToBlack,
};


// Channel definitions
//
// Each stick is represented by a MIDI channel. These are mapped to particular pins
// around the Teensy from pin number 02 (the lowest) to pin 23 (highest), with gaps
// in the middle due to the way "ports" are mapped to pins internally on the Teensy

// Pin layouts on the teensy 3/3.1:
// PORTDC: 02, 14, 07, 08, 06, 20, 21, 05, 15, 22, 23, 09, 10, 13, 11, 12
//    AKA: 02, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 20, 21, 22, 23

static uint8_t pinIndex[] = { 0, 7, 4, 2, 3, 11, 12, 14, 15, 13, 1, 8, 5, 6, 9, 10 };    
    
Channel channel0(0, leds + (pinIndex[0] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel1(1, leds + (pinIndex[1] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel2(2, leds + (pinIndex[2] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel3(3, leds + (pinIndex[3] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel4(4, leds + (pinIndex[4] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel5(5, leds + (pinIndex[5] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel6(6, leds + (pinIndex[6] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel7(7, leds + (pinIndex[7] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel8(8, leds + (pinIndex[8] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channel9(9, leds + (pinIndex[9] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channelA(10, leds + (pinIndex[10] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channelB(11, leds + (pinIndex[11] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channelC(12, leds + (pinIndex[12] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channelD(13, leds + (pinIndex[13] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channelE(14, leds + (pinIndex[14] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);
Channel channelF(15, leds + (pinIndex[15] * NUM_LEDS_PER_CHANNEL), NUM_LEDS_PER_CHANNEL, palettes, PALETTE_COUNT);

Channel* channels[] = {
  &channel0, &channel1, &channel2, &channel3, &channel4, &channel5, &channel6, &channel7,
  &channel8, &channel9, &channelA, &channelB, &channelC, &channelD, &channelE, &channelF,
};

void setup() {
//  Serial.begin(115200);

  delay(2000);

  LEDS.addLeds<WS2811_PORTDC, NUM_CHANNELS>(leds, NUM_LEDS_PER_CHANNEL);

  usbMIDI.setHandleNoteOff(OnNoteOff);
  usbMIDI.setHandleNoteOn(OnNoteOn);
  usbMIDI.setHandleVelocityChange(OnVelocityChange);
  usbMIDI.setHandleControlChange(OnControlChange);
  usbMIDI.setHandleProgramChange(OnProgramChange);
  usbMIDI.setHandleAfterTouch(OnAfterTouch);
  usbMIDI.setHandlePitchChange(OnPitchChange);  
}

unsigned long loopStartMillis = 0;
unsigned long loopStartDelta = 0;

void loop() {
//  Serial.println("loop()");
  loopStartMillis = millis();
  while (usbMIDI.read());
  
  for (int channel = 0; channel < NUM_CHANNELS; channel++) {
    channels[channel]->draw();
  }

  setMasterBrightness();

  FastLED.show();
  loopStartDelta = millis() - loopStartMillis;
  if (loopStartDelta < 1000 / FRAMES_PER_SECOND) {
      LEDS.delay(1000 / FRAMES_PER_SECOND - loopStartDelta);
  } 
}

void setMasterBrightness() {
  FastLED.setBrightness(map(analogRead(BRIGHTNESS_PIN), 0, 1023, 0, 255));
}

void OnNoteOn(byte channel, byte note, byte velocity) {
//  Serial.print("OnNoteOn(channel = "); Serial.print(channel);
//  Serial.print(", note = "); Serial.print(note);
//  Serial.print(", velocity = "); Serial.print(velocity);
//  Serial.println(")");
  
  channels[channel - 1]->noteOn(note, velocity);
}

void OnNoteOff(byte channel, byte note, byte velocity) {
//  Serial.print("OnNoteOff(channel = "); Serial.print(channel);
//  Serial.print(", note = "); Serial.print(note);
//  Serial.print(", velocity = "); Serial.print(velocity);
//  Serial.println(")");

  channels[channel - 1]->noteOff(note, velocity);  
}

void OnVelocityChange(byte channel, byte note, byte velocity) {
//  Serial.print("OnVelocityChange(channel = "); Serial.print(channel);
//  Serial.print(", velocity = "); Serial.print(velocity);
//  Serial.println(")");
  channels[channel - 1]->velocityChange(note, velocity);
}

void OnControlChange(byte channel, byte control, byte value) {
//  Serial.print("OnControlChange(channel = "); Serial.print(channel);
//  Serial.print(", control = "); Serial.print(control);
//  Serial.print(", value = "); Serial.print(value);
//  Serial.println(")");
  channels[channel - 1]->controlChange(control, value);
}

void OnProgramChange(byte channel, byte program) {
//  Serial.print("OnProgramChange(channel = "); Serial.print(channel);
//  Serial.print(", program = "); Serial.print(program);
//  Serial.println(")");

  channels[channel - 1]->programChange(program);
}

void OnAfterTouch(byte channel, byte pressure) {
//  Serial.print("OnAfterTouch(channel = "); Serial.print(channel);
//  Serial.print(", pressure = "); Serial.print(pressure);
//  Serial.println(")");

  channels[channel - 1]->afterTouch(pressure);
}

void OnPitchChange(byte channel, int pitch) {
//  Serial.print("OnPitchChange(channel = "); Serial.print(channel);
//  Serial.print(", pitch = "); Serial.print(pitch);
//  Serial.println(")");

  channels[channel - 1]->pitchChange(pitch);  
}

