#ifndef SolidBars_h
#define SolidBars_h

#include "FastLED.h"
#include "Program.cpp"

class SolidBars : public Program {
  
  private:
    uint8_t paletteIndex;
    uint8_t brightness;
    boolean on;
    uint8_t fade;
  
  public:
    SolidBars() : on(false), fade(0) {}
    
    void draw(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) {
      if (on) {
        fill_solid(leds, numLeds, ColorFromPalette(palette, paletteIndex, brightness));
      } else if (fade > 0) {
        fill_solid(leds, numLeds, ColorFromPalette(palette, paletteIndex, brightness));  
        nscale8_video(leds, numLeds, fade--);
      } else {
        fill_solid(leds, numLeds, CRGB::Black);
      }
    }
    
    void noteOn(byte note, byte velocity) {
      on = true;
      paletteIndex = note << 1;
      brightness = velocity << 1;
    }
    
    void noteOff(byte note, byte velocity) {
      on = false;
      fade = velocity << 1;
    }

    void velocityChange(byte note, byte velocity) {
    }

    void controlChange(byte control, byte value) {
    }

    void afterTouch(byte pressure) {
    }

    void pitchChange(int pitch) {
    }

};

#endif





