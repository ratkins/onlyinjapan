#ifndef Heights_h
#define Heights_h

#include "FastLED.h"
#include "Program.cpp"

class Heights : public Program {
  
  private:
    uint8_t height;
    uint8_t paletteIndex;
  
  public:
    Heights() : height(0), paletteIndex(0) {}
    
    void draw(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) {
      if (height > 0) {
        for (int i = 0; i < height; i++) {
          leds[i] = ColorFromPalette(palette, paletteIndex);
          leds[numLeds - i - 1] = ColorFromPalette(palette, paletteIndex);
        }
      } else {
        memset8(leds, 0, numLeds * 3);
      }
    }
    
    void noteOn(byte note, byte velocity) {
      paletteIndex = note;
      height = map(velocity, 0, 127, 0, 80);
    }
    
    void noteOff(byte note, byte velocity) {
      height = 0;
    }

    void velocityChange(byte note, byte velocity) {
    }

    void controlChange(byte control, byte value) {
    }

    void afterTouch(byte pressure) {
    }

    void pitchChange(int pitch) {
    }

};

#endif



