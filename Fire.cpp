#ifndef Fire_h
#define Fire_h

#include "FastLED.h"
#include "Program.cpp"

#define HEIGHT 80

// Adapted from https://github.com/FastLED/FastLED/blob/FastLED3.1/examples/Fire2012/Fire2012.ino
class Fire : public Program {
  
  private:
    uint8_t cooling;
    uint8_t sparking;
    uint8_t heat0[HEIGHT];
    uint8_t heat1[HEIGHT];
  
  public:
    Fire() : cooling(55), sparking(120) {}
    
    void draw(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) {
      // Add entropy to random number generator; we use a lot of it.
      random16_add_entropy(random());

      // Step 1.  Cool down every cell a little
      for (int i = 0; i < HEIGHT; i++) {
        heat0[i] = qsub8(heat0[i], random8(0, ((cooling * 10) / HEIGHT) + 2));
        heat1[i] = qsub8(heat1[i], random8(0, ((cooling * 10) / HEIGHT) + 2));        
      }
  
      // Step 2.  Heat from each cell drifts 'up' and diffuses a little
      for (int k = HEIGHT - 1; k >= 2; k--) {
        heat0[k] = (heat0[k - 1] + heat0[k - 2] + heat0[k - 2] ) / 3;
        heat1[k] = (heat1[k - 1] + heat1[k - 2] + heat1[k - 2] ) / 3;        
      }
    
      // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
      if (random8() < sparking) {
        int y = random8(7);
        heat0[y] = qadd8(heat0[y], random8(160, 255));
      }
      if (random8() < sparking) {
        int y = random8(7);
        heat1[y] = qadd8(heat1[y], random8(160, 255));
      }

      // Step 4.  Map from heat cells to LED colors
      for (int j = 0; j < HEIGHT; j++) {
        leds[j] = HeatColor(heat0[j]);
        leds[numLeds - j - 1] = HeatColor(heat1[j]);
      }
    }
    
    void noteOn(byte note, byte velocity) {
      // COOLING: How much does the air cool as it rises?
      // Less cooling = taller flames.  More cooling = shorter flames.
      // Default 50, suggested range 20-100 
      cooling = note;

      // SPARKING: What chance (out of 255) is there that a new spark will be lit?
      // Higher chance = more roaring fire.  Lower chance = more flickery fire.
      // Default 120, suggested range 50-200.
      sparking = velocity;
    }
    
    void noteOff(byte note, byte velocity) {
      cooling = 255;
      sparking = 0;
    }

    void velocityChange(byte note, byte velocity) {
    }

    void controlChange(byte control, byte value) {
    }

    void afterTouch(byte pressure) {
    }

    void pitchChange(int pitch) {
    }

};

#endif


