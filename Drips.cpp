#ifndef Drips_h
#define Drips_h

#include "FastLED.h"
#include "Program.cpp"

#define HEIGHT 80

class Drips : public Program {
  
  private:
    boolean on;
    boolean down;
    uint8_t paletteIndex;
    uint8_t length;
    int8_t startPos;
    uint8_t fade;
    
  public:
    Drips(boolean down) : on(false), down(down), paletteIndex(0), length(16), startPos(-length) {}
    
    void draw(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) {
      fill_solid(leds, numLeds, CRGB::Black);      
      if (on) {
        updateDrip(leds, numLeds, palette);
      } else if (fade > 0) {
        updateDrip(leds, numLeds, palette);  
        nscale8_video(leds, numLeds, fade--);
      } else {
        fill_solid(leds, numLeds, CRGB::Black);
      }
    }
    
    void updateDrip(CRGB* leds, uint8_t numLeds, CRGBPalette16 palette) {
      for (int i = 0; i < length; i++) {
        if (startPos + i >= 0 && startPos + i < HEIGHT) {
          if (down) {
            leds[HEIGHT - startPos - i - 1] = ColorFromPalette(palette, paletteIndex) %= i * (255 / length);
            leds[HEIGHT + startPos + i] = ColorFromPalette(palette, paletteIndex) %= i * (255 / length);            
          } else {
            leds[startPos + i] = ColorFromPalette(palette, paletteIndex) %= i * (255 / length);
            leds[numLeds - (startPos + i) - 1] = ColorFromPalette(palette, paletteIndex) %= i * (255 / length);
          }
        }
      }
      startPos++;      
    }
    
    void noteOn(byte note, byte velocity) {
      on = true;
      length = velocity >> 1;
      startPos = -length;
      paletteIndex = note << 1;
    }
    
    void noteOff(byte note, byte velocity) {
      on = false;
      fade = velocity << 1;
    }

    void velocityChange(byte note, byte velocity) {
    }

    void controlChange(byte control, byte value) {
    }

    void afterTouch(byte pressure) {
    }

    void pitchChange(int pitch) {
    }

};

#endif



